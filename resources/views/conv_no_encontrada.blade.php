@extends('layouts.wvlayout')

@section('content')

<link href="/css/conv_no_encontrada.css" rel="stylesheet">

<div class="container">
<!--Jumbotron-->
<div class="jumbotron">

    <h1 class="h1-reponsive mb-3 red-text"><strong>Conversación no encontrada</strong></h1>
    <p class="lead">Parece que la conversación solicitada no se encuentra en la base de datos.</p>
    <hr class="my-4">
    <p>Es posible que la duración de la carga haya expirado.</p>
    <p>Puede cargar un archivo de texto desde el menú principal o enviarlo por mail a <a href="mailto:whatsappvisum@gmail.com">whatsappvisum@gmail.com</a></p>
    <p class="lead">
        <a class="btn btn-danger" href="/" role="button">Volver al menú principal</a>
    </p>

</div>
<!--Jumbotron-->
</div>

@endsection