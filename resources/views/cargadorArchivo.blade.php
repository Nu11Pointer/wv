@extends('layouts.wvlayout')

@section('content')

<div class="container">
<div class="row top-buffer bottom-buffer text-center">
<div class="card col-md-4 col-md-offset-4" style="background-color: #C5E1A5;">
	<h2>Envía la conversación a<h3><a href="mailto:whatsappvisum@gmail.com">whatsappvisum@gmail.com</a></h3></h2>
	<hr>

	{!! Form::open(['url' => 'cargarConversacion', 'files' => true, 'enctype' => "multipart/form-data"]) !!}
	<h3>O sube el archivo de texto</h3>
	<div class="btn btn-outline-success waves-effect">
	{!! Form::file('conversacion') !!}
	</div>
	<div class= "form-group">
	{!! Form::submit('Agregar elemento', ['class' => 'btn btn-success']) !!}
	</div>
	{!! Form::close() !!}
</div>
</div>
</div>

<div class="container">
@include('conv_cargadas')
</div>

@endsection