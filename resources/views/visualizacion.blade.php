@extends('layouts.wvlayout')

@section('content')
<script src='/js/globalVisualizacion.js'></script>

<div id="mySidenav" class="sidenav" style="width: 0px;">
	<div id="legend" class = "padding-min"></div>
</div>

<button type="button" class="btn btn-primary button-fijo" onclick="navOpenClose()">
	<i class="fa fa-eye" aria-hidden="true"></i> Leyenda
</button>

<div class="container">
	<div class="row">
		<h1 class="text-center conteo"><i class="fa fa-send"></i> Se han procesado <strong class="count">{{$cant_msg}}</strong> mensajes</h1>
	</div>
	
	<h1 class="text-center"><strong>Σ</strong> Visualizaciones según la cantidad</h1>

	<div class="alert alert-success alert-dismissable text-center" hidden>
	  En este gráfico de torta se puede observar cual de todos los remitentes envió más mensajes. Esto no se debe confundir con el hecho de que alguien "hable más", sino que simplemente se muestra la cantidad de mensajes unitarios enviados por cada uno. No se toma en cuenta la longitud de los mismos, por lo que cada uno puede variar entre un caracter y cientos.
	</div>

	<div class="row bottom-buffer">
		<div class="col-md-12">
			<h2><i class="fa fa-pie-chart" aria-hidden="true"></i> Cantidad de mensajes enviados</h2>
			<canvas id="tortaCantMsg"></canvas>
		</div>
	</div>

	<div class="row bottom-buffer">
		<div class="col-md-12">
			<h2><i class="fa fa-bar-chart" aria-hidden="true"></i> Cantidad de mensajes por remitente</h2>
			<canvas id="barrasCantMsg"></canvas>
		</div>
	</div>

	<div class="row bottom-buffer">
		<div class="col-md-8 col-md-offset-2">
			<h2><i class="fa fa-pie-chart" aria-hidden="true"></i> Longitud de mensajes</h2>
			<canvas id="polarLargoPorRemitente"></canvas>
		</div>
	</div>

	<div class="row bottom-buffer">
		<h2><i class="fa fa-circle-o-notch" aria-hidden="true"></i> Comparación</h2>
		<div class="col-md-6">
			<h3>Cantidad</h3>
			<canvas id="doughnutCantMsg"></canvas>
		</div>
		<div class="col-md-6 bottom-buffer">
			<h3>Longitud</h3>
			<canvas id="doughnutLargoPorRemitente"></canvas>
		</div>
		<div class="col-md-12">
			<h3><i class="fa fa-bar-chart" aria-hidden="true"></i> Comparación ratio porcentual</h3>
			<canvas id="barrasComparacion"></canvas>
			<div id="legend-barrasComparacion" class="col-md-4 pull-right"></div>
		</div>
	</div>

	<h1 class="text-center"><i class="fa fa-clock-o" aria-hidden="true"></i> Visualizaciones según el tiempo</h1>

	<div class="row bottom-buffer">
		<div class="col-md-12">
			<h2><i class="fa fa-calendar" aria-hidden="true"></i> Cantidad de mensajes por mes</h2>
			<canvas id="ondasCantPorMes"></canvas>
		</div>
	</div>	

	<div class="row bottom-buffer">
		<h2><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> Por dias de las semana</h2>
		<div class="col-md-12">
			<h3>En total</h3>
			<canvas id="radarDiasSemanaTotal"></canvas>
		</div>
		<div class="col-md-12">
			<h3><i class="fa fa-group" aria-hidden="true"></i> Por remitente</h3>
			<canvas id="radarDiasSemanaRemitente"></canvas>
		</div>
	</div>

	<div class="row bottom-buffer">
		<div class="col-md-12 bottom-buffer">
			<h2><i class="fa fa-area-chart" aria-hidden="true"></i> Mensajes enviados según la hora</h2>
			<canvas id="lineaPorHora"></canvas>
		</div>
		<div class="col-md-12 bottom-buffer">
			<h2><i class="fa fa-area-chart" aria-hidden="true"></i> Según la hora por remitente</h2>
			<canvas id="lineaPorHoraRemitente"></canvas>
		</div>
	</div>	

	<div class="row bottom-buffer">
		<div class="col-md-12 text-center">
			<h2><i class="fa fa-braille" aria-hidden="true"></i> Heatmap desde un año atrás hasta hoy ...</h2>
			<div class="heatmap"></div>
		</div>
	</div>	

	<h1 class="text-center"><i class="fa fa-commenting-o" aria-hidden="true"></i> Visualizaciones según el contenido</h1>

	<div class="row bottom-buffer">
		<div class="col-md-12">
			<h2>Respuestas positivas <i class="fa fa-check" aria-hidden="true"></i> vs negativas <i class="fa fa-times" aria-hidden="true"></i></h2>
			<canvas id="tortaSiNo"></canvas>
			<div id="legend-tortaSiNo" class="col-md-4 pull-right"></div>
		</div>
	</div>

	<div class="row bottom-buffer">
		<div class="col-md-12">
			<h2><i class="fa fa-bar-chart" aria-hidden="true"></i> Top 50 ranking palabras</h2>
			<canvas id="barrasConteoPalabras"></canvas>
		</div>
		<div class="col-md-12">
			<h2><i class="fa fa-bar-chart" aria-hidden="true"></i> Top 50 ranking sin stopwords</h2>
			<canvas id="barrasConteoSinStopwords"></canvas>
		</div>
		<div class="col-md-12">
			<h2><i class="fa fa-smile-o" aria-hidden="true"></i> Emojis</h2>
			<canvas id="barrasEmoji"></canvas>
			<div style="text-align:justify; padding-left: 30px;" id="legend-barrasEmoji"></div>
		</div>
	</div>	

	<div class="bubbleChart"/>

</div> <!-- end container -->

<!-- JAVASCRIPT -->

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/css/mdb.css" rel="stylesheet">
<script src='https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/js/mdb.js'></script> CARGA ARRIBA -->

<script src='/js/palette.js'></script>
<script src="//twemoji.maxcdn.com/twemoji.min.js"></script>
<!-- <script src='/js/palette-material.js'></script> -->
@include('graficos.colores')

@include('graficos.tortaCantMsg')
@include('graficos.ondasCantPorMes')
@include('graficos.barrasComparacion')
@include('graficos.radarDiasSemanaTotal')
@include('graficos.radarDiasSemanaRemitente')
@include('graficos.tortaSiNo')
@include('graficos.barrasConteoPalabras')
@include('graficos.barrasConteoSinStopwords')

<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js'></script>

@include('graficos.doughnutCantMsg')
@include('graficos.doughnutLargoPorRemitente')
@include('graficos.polarLargoPorRemitente')
@include('graficos.barrasCantMsg')
@include('graficos.lineaPorHora')
@include('graficos.lineaPorHoraRemitente')
@include('graficos.barrasEmoji')

<link rel="stylesheet" type="text/css" href="/css/calendar-heatmap.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js" charset="utf-8"></script>
<script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script src="/js/calendar-heatmap.js"></script>

@include('graficos.heatMapAnio')

@endsection