<!DOCTYPE html>
<meta charset="utf-8">
<head>
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
</body>

<script src="https://d3js.org/d3.v4.min.js"></script>

<script>

agregarBarras("porFecha");
// agregarBarras("porDia");

function agregarBarras(urlPath){
	var request = new XMLHttpRequest();
	request.open("GET", "./"+urlPath);
	request.responseType = "json";
	request.send();
	request.onload = function() { //lo que ocurre al recibir la respuesta del servidor
	  dibujarBarras(request.response);
	  };
}

function dibujarBarras(arrayJson, anchoBarra = 5){

	var anchoBarra = 15;
	var xCorrimiento = 50;
	var yCorrimiento = 50;

	var width = 1000 + xCorrimiento
	var height = arrayJson.length*(anchoBarra+1) + yCorrimiento + 100; //100 margen de error

	var dataArray = arrayDeAtributo(arrayJson, "cantidad");
	//var dataArray = [775, 503, 186, 168, 65, 47, 39, 138, 133, 382, 620, 585, 870, 1270, 1053, 695, 651, 730, 1039, 926, 939, 710, 1304, 1167];

	var scaleArray = arrayDeAtributo(arrayJson, "fecha");

	var widthScale = d3.scaleLinear()
						.domain([0, max(dataArray)])
						.range([0, width]);

	var colorVerdes = d3.scaleLinear()
					.domain([0, max(dataArray)])
					.range(["#9CCC65", "#33691E"]);

	var colorDayNight = d3.scaleLinear()
					.domain([0, 23])
					.range(["#212121", "#FFC107"]); //De mas negro a mas claro

	/*axisScale = d3.scaleLinear()
					.domain([0, dataArray.length])
					.range([0, (dataArray.length+1)*anchoBarra]);	*/

	axisScale = d3.scalePoint()
					.domain(scaleArray)
					.range([0, dataArray.length*(anchoBarra+1)]);

	var axis = d3.axisLeft()
					.scale(axisScale);

	var grafico = d3.select("html")
					.append("svg")
					.attr("width",width)
					.attr("height",height)
					.append("g")
					.attr("transform", "translate("+xCorrimiento+", "+yCorrimiento+")")
					.call(axis);

	var bars = grafico.selectAll("rect")
					.data(dataArray)
					.enter()
						.append("rect")
						.attr("width", function(d) {return widthScale(d);})
						.attr("height", anchoBarra)
						.attr("y", function(d, i) {return i * (anchoBarra+1);})
						.attr("fill", function(d){ return colorVerdes(d)})
						//.attr("fill", function(d, i){ return colorDayNight( -(1/4)*((i-12)*(i-12)) +23 )})
						.append("svg:title")
						.text(function(d){ return d;});
}

function max(numArray) {
  return Math.max.apply(null, numArray);
}

function arrayDeAtributo(arreglo, atributo){
	toReturn = [];
	for (i = 0; i < arreglo.length; i++)
	    toReturn.push(arreglo[i][atributo]);
	return toReturn;
}

</script>