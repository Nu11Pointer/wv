
<h1>Conversaciones cargadas</h1>
@foreach ($conv as $c)
 <ul class="list-group col-md-3">
 <a href="/visualizacion/{{ $c->id_conv }}">
 	<li class="list-group-item list-group-item-success" ">Con <i>{{ $c->nombre_conv }}</i> : <b>{{$c->cant}} mensajes</b></li>
 </a>
 	@foreach ($c->remitentes as $r)
  		<li class="list-group-item">{{ $r }}</li>
  	@endforeach
</ul>
@endforeach