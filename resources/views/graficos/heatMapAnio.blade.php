<script type="text/javascript">
$(function () {

  function getRawData(){
    $.ajaxSetup({
      async: false
    });

    _data = {};
    $.getJSON("/cantAnioAtras/"+"{{$id_conv}}", function( data ) { 
        data.forEach(function(d) {
          _data[d.fecha] = d.count;
        });
    });
   
    return _data;
  }

  function dateElementToMySQL(dateElement){
    //fechaCorta = dateElement.toLocaleDateString();
    diamesanio = dateElement.toLocaleDateString().split("/");

    if(diamesanio[1].length == 1) //Los meses tienen que ser 01 , 02 , 03 ...
      diamesanio[1] = "0"+diamesanio[1];

    if(diamesanio[0].length == 1) //Los dias también
      diamesanio[0] = "0"+diamesanio[0];

    return(diamesanio[2]+"-"+diamesanio[1]+"-"+diamesanio[0]);
  }

  var cuenta = getRawData();
  var now = moment().endOf('day').toDate();
  var yearAgo = moment().startOf('day').subtract(1, 'year').toDate();

  var chartData = d3.time.days(yearAgo, now).map(function (dateElement) {
    fecha = dateElementToMySQL(dateElement);
    return {
      date: dateElement,
      count: cuenta[fecha] ? cuenta[fecha] : 0
    };
  });

  var heatmap = calendarHeatmap()
                  .data(chartData)
                  .selector('.heatmap')
                  .tooltipEnabled(true)
                  .colorRange(['#F1F8E9', '#33691E'])
                  .onClick(function (data) {
                    console.log('data', data);
                  });
  heatmap();  // render the chart
});
</script>