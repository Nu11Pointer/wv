<script>
(function ($) { 

    function getRawData(){;
        var rawData = [];
        $.getJSON("/vsSiNo/"+"{{$id_conv}}", function( data ) {
            data.forEach(function(d) {
                rawData.push({label: d.palabra, value: d.cantidad, color: d.palabra=="si"? "#4CAF50" : "#F44336"});
            });
        });
        return rawData;
    }

    var option = {
    responsive: true,
    segmentStrokeWidth: 0,
    scaleLineWidth: 0,
    };
   
    // Get the context of the canvas element we want to select
    var ctx = document.getElementById("tortaSiNo").getContext('2d');
    myLineChart = new Chart(ctx).Pie(getRawData(), option);

    document.getElementById("legend-tortaSiNo").appendChild( decorarLegend(myLineChart.generateLegend()) );
}(jQuery));

</script>