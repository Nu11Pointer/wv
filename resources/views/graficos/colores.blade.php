<script>

	var color = {};
	function generarColores() {

        var remitentes;
        $.getJSON("/remitentes/"+"{{$id_conv}}", function( data ) {	
            remitentes = data;
        });

        var materialColors = ["#F44336","#E91E63","#9C27B0","#673AB7","#3F51B5","#2196F3","#03A9F4","#00BCD4","#009688","#4CAF50","#8BC34A","#CDDC39","#FFEB3B","#FFC107","#FF9800","#FF5722","#795548","#9E9E9E","#607D8B" /*,#000000,"#FFFFFF"*/];
        if(remitentes.length == 2){
        	color[remitentes[0]] = materialColors[Math.floor(Math.random() * materialColors.length)];
        	color[remitentes[1]] = materialColors[Math.floor(Math.random() * materialColors.length)];
        }
        else{
			var paleta = palette('tol-rainbow', remitentes.length);

			remitentes.forEach(function(r) {
				var c=paleta.pop();
	        	color[r] = "#"+c;
        	});
        };
	}

	generarColores();

</script>