<script>
(function ($) { 
    function getRawData(){
        var _labels = [];
        var _cant = [];
        var _long = [];

        $.getJSON("/porcCantLong/"+"{{$id_conv}}", function( data ) {
            data.forEach(function(d) {
                _labels.push(d.remitente);
                _cant.push(parseFloat(d.porc_count));
                _long.push(parseFloat(d.porc_long));
            });
        });
        return {labels: _labels, cant:_cant, long: _long};
    }

    var option = {
    responsive: true,
    };
   

    var rawData = getRawData();
    var data = {
        labels: rawData.labels,
        datasets: [
            {
                label: "Cantidad de mensajes",
                fillColor: "rgba(139, 195, 74, 0.5)",
                strokeColor: "rgba(139, 195, 74, 0.8)",
                highlightFill: "rgba(139, 195, 74, 1)",
                highlightStroke: "rgba(139, 195, 74, 1)",
                data: rawData.cant
            },
            {
                label: "Longitud de mensajes",
                fillColor: "rgba(76, 175, 80, 0.5)",
                strokeColor: "rgba(76, 175, 80, 0.8)",
                highlightFill: "rgba(76, 175, 80, 1)",
                highlightStroke: "rgba(76, 175, 80, 1)",
                data: rawData.long
            }
        ]
    };

    // Get the context of the canvas element we want to select
    var ctx = document.getElementById("barrasComparacion").getContext('2d');
    var myLineChart = new Chart(ctx).Bar(data, option); //'Line' defines type of the chart.

    document.getElementById("legend-barrasComparacion").appendChild( decorarLegend(myLineChart.generateLegend()) );
}(jQuery));
</script>