<script>
(function ($) { 
    function getRawData(){
        var rawData = [];
        $.getJSON("/porDiaRemitente/"+"{{$id_conv}}", function( data ) {

            _labels = [];
            _categorias = {};
            data.forEach(function(d) {
                _labels.push(d.dia); //Los dias se van a repetir, luego hacer unique
                if (_categorias[d.remitente] != null) 
                    _categorias[d.remitente].push(d.cantidad)
                else _categorias[d.remitente]=[d.cantidad];
            });
        });

        _datasets = [];
        for(remitente in _categorias)
            _datasets.push(
            {
                label: remitente,
                fillColor: hexToRgbA(color[remitente])+",0.1)",
                strokeColor: color[remitente],
                pointColor: color[remitente],
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: color[remitente],
                data: _categorias[remitente],
            }
            );

        return {labels: $.unique(_labels), datasets: _datasets};
    }

	rawData = getRawData();
    
    var option = {
        responsive: true,
    };

	var data = {
    	labels: rawData.labels,
    	datasets: rawData.datasets
	};

    var ctx = document.getElementById("radarDiasSemanaRemitente").getContext('2d');
    var myLineChart = new Chart(ctx).Radar(data, option);
}(jQuery));
</script>