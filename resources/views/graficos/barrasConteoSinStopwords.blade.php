<script>
(function ($) { 
    function getRawData(){
        var _labels = [];
        var _cant = [];
        
        $.getJSON("/conteoSinStopwords/"+"{{$id_conv}}", function( data ) {
            data.forEach(function(d) {
                _labels.push(d.palabra);
                _cant.push(d.total);
            });
        });
        return {labels: _labels, cant:_cant};
    }

    var option = {
    responsive: true,
    };
   

    var rawData = getRawData();
    var data = {
        labels: rawData.labels,
        datasets: [
            {
                label: "Ranking palabras",
                fillColor: "rgba(27, 94, 32, 0.5)",
                strokeColor: "rgba(27, 94, 32, 0.8)",
                highlightFill: "rgba(27, 94, 32, 1)",
                highlightStroke: "rgba(27, 94, 32, 1)",
                data: rawData.cant
            }
        ]
    };

    // Get the context of the canvas element we want to select
    var ctx = document.getElementById("barrasConteoSinStopwords").getContext('2d');
    var myLineChart = new Chart(ctx).Bar(data, option);

}(jQuery));
</script>