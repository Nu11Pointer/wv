<script>
$(function () {
    function getRawData(){
        $.ajaxSetup({
          async: false
        });

        _labels = [];
        _data = [];
        _color = [];
        $.getJSON("/cantPorRemitente/"+"{{$id_conv}}", function( data ) {  
            data.forEach(function(d) {
                _labels.push(d.remitente);
                _data.push(d.cantidad);
                _color.push(color[d.remitente]);
            });
        });
       
        return {labels: _labels, data: _data, color:_color};
    }

    rawData = getRawData();

    var ctx = document.getElementById('doughnutCantMsg').getContext('2d');

    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'doughnut',

        // The data for our dataset
        data: {
            labels: rawData.labels,
            datasets: [{
                label: "Cantidad de mensajes",
                borderColor: 'rgba(33, 33, 33, 0.3)',
                data: rawData.data,
                backgroundColor: rawData.color,
            }]
        },

        // Configuration options go here
        options: {}
    });
});

</script>