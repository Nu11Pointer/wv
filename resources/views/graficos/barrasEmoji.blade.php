<script>
(function ($) { 
    function getRawData(){
        var _labels = [];
        var _data = [];

        $.getJSON("/sumaEmoji/"+"{{$id_conv}}", function( data ) {
            data.forEach(function(d) {
                _labels.push(d.emoji);
                _data.push(d.total);
            });
        });
        return {labels: _labels, data:_data};
    }

    var rawData = getRawData();

    var ctx = document.getElementById('barrasEmoji').getContext('2d');
    var chart = new Chart(ctx, {

        type: 'bar',
        data: {
            labels: rawData.labels,
            datasets: [{
                label: "Emoji",
                backgroundColor: 'rgb(255, 193, 7)',
                borderColor: 'rgba(255, 235, 59, 1)',
                data: rawData.data,
            }]
        },
        options: {
            scales: {
                xAxes: [{ barPercentage: 1 }],
                yAxes: [{
                    display: true,
                    ticks: {
                        //suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                        // OR //
                        beginAtZero: true,   // minimum value will be 0.
                        /*fontSize: 20,
                        stepSize: 1*/
                    }
                }]
            }
        }
    });

    var legend = document.getElementById("legend-barrasEmoji").innerHTML = rawData.labels.join(" ")+"<span style='width:100%;display:inline-block;'></span>";
    twemoji.parse(document.getElementById("legend-barrasEmoji"));

}(jQuery));
</script>