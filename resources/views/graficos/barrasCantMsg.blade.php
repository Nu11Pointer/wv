<script>
$(function () {
    function getRawData(){
        $.ajaxSetup({
          async: false
        });

        _labels = [];
        _data = [];
        _color = [];
        $.getJSON("/cantPorRemitente/"+"{{$id_conv}}", function( data ) {	
            data.forEach(function(d) {
                _labels.push(d.remitente);
                _data.push(d.cantidad);
                _color.push(color[d.remitente]);
            });
        });
       
        return {labels: _labels, data: _data, color:_color};
    }

    rawData = getRawData();

	var ctx = document.getElementById('barrasCantMsg').getContext('2d');

	var chart = new Chart(ctx, {
	    // The type of chart we want to create
	    type: 'bar',

	    // The data for our dataset
	    data: {
	        labels: rawData.labels,
	        datasets: [{
	            label: "Cantidad de mensajes",
	            borderColor: 'rgb(255, 99, 132)',
	            data: rawData.data,
	            backgroundColor: rawData.color,
	        }]
	    },

	    // Configuration options go here
	    options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true   // minimum value will be 0.
                    }
                }]
            }
        }
	});
});

</script>