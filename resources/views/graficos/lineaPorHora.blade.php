<script>
$(function () {
    function getRawData(){
        var _hora_cant = {};
        var _data = [];
        var _labels = []; //De 0 a 23
        $.getJSON("/porHora/"+"{{$id_conv}}", function( data ) {	
            data.forEach(function(d) {
                _hora_cant[d.hora] = d.cantidad;
            });
        });
       	
		for (var i = 0; i <= 23; i++) {
			_labels.push(i);
			if(i in _hora_cant)
				_data[i]=_hora_cant[i];
			else _data[i] = 0
		}
        return {labels: _labels, data: _data};
    }

    var rawData = getRawData();

	var ctx = document.getElementById('lineaPorHora').getContext('2d');

	var chart = new Chart(ctx, {
	    // The type of chart we want to create
	    type: 'line',

	    // The data for our dataset
	    data: {
	        labels: rawData.labels,
	        datasets: [{
	            label: "Mensajes por hora",
	            borderColor: /*'rgb(139, 195, 74)'*/'rgb(51, 105, 30)',
	            data: rawData.data,
	            backgroundColor: /*"rgba(174, 213, 129, 0.5)"*/'rgba(139, 195, 74, 0.5)',
	            //lineTension: 0,
	        }]
	    },

	    // Configuration options go here
	    options: {
	    	steppedLine :true
	    }
	});
});

</script>