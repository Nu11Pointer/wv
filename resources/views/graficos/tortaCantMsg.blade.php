<script>
(function ($) { 

    function getRawData(){
        $.ajaxSetup({
          async: false
        });

        var rawData = [];
        $.getJSON("/cantPorRemitente/"+"{{$id_conv}}", function( data ) {
            data.forEach(function(d) {
                /*
                value = cantidad
                color
                highlight
                label = remitente
                */
                rawData.push({label: d.remitente, value: d.cantidad, color: color[d.remitente]});
            });
        });
        return rawData;
    }

    var option = {
    responsive: true,
    };
   
    // Get the context of the canvas element we want to select
    var ctx = document.getElementById("tortaCantMsg").getContext('2d');
    var myLineChart = new Chart(ctx).Pie(getRawData(), option);

    document.getElementById("legend").appendChild( decorarLegend(myLineChart.generateLegend()) );
}(jQuery));

</script>