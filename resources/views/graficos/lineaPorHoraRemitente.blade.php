<script>
$(function () {
    function getRawData(){
        _hora_cant = {};
        var setRemit = new Set();
        $.getJSON("/porHoraRemitente/"+"{{$id_conv}}", function( data ) {	
            data.forEach(function(d) {
            	var remitente = d.remitente;
            	setRemit.add(remitente);
            	if(_hora_cant[d.hora]) //Si ya fue agregado un remitente, hay que agregar otro
               		_hora_cant[d.hora][remitente] = d.cantidad;
               	else _hora_cant[d.hora] = {[remitente] : (d.cantidad)}; 
            });
        });
        
        //Falta completar las horas que no habló nadie
        for(var i=0; i<=23; i++){
        	if(! _hora_cant[i]){
        		_hora_cant[i]={};
        		setRemit.forEach(function(r){
        			_hora_cant[i][r] = 0;
        		});
        	}
        }

        var dataPorRemit = {};
       	setRemit.forEach(function (r){
       		dataPorRemit[r]=[];
       	});

        var _labels = []; //De 0 a 23
		for (var i = 0; i <= 23; i++) {
			_labels.push(i);
			setRemit.forEach(function(r){
				dataPorRemit[r].push(_hora_cant[i][""+r]);
			});
		}

        var _datasets = [];
		for(var remitente in dataPorRemit)
			_datasets.push(
			{
	            label: remitente,
	            data: dataPorRemit[remitente],
	            borderColor: color[remitente],
	            backgroundColor: hexToRgbA(color[remitente])+", 0.2)",
        	}
	    	);

        return {labels: _labels, datasets: _datasets};
    }

    var rawData = getRawData();

	var ctx = document.getElementById('lineaPorHoraRemitente').getContext('2d');

	var chart = new Chart(ctx, {
	    // The type of chart we want to create
	    type: 'line',
	    // The data for our dataset
	    data:
			{
		        labels: rawData.labels,
		        datasets: rawData.datasets,
	    	},
	    // Configuration options go here
	    options: {
	    	steppedLine :true
	    }
	});
});

</script>