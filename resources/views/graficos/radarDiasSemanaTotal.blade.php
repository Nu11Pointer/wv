<script>
(function ($) { 
    function getRawData(){
        $.ajaxSetup({
          async: false
        });

        var rawData = [];
        $.getJSON("/porDia/"+"{{$id_conv}}", function( data ) {

            _labels = [];
            _data = [];
            data.forEach(function(d) {
                _labels.push(d.dia);
                _data.push(d.cantidad);
            });
        });
        return {labels: _labels, data:_data};
    }

	rawData = getRawData();

    var option = {
    responsive: true,
    };

	var data = {
	labels: rawData.labels,
	datasets: [
	    {
	        label: "My First dataset",
	        fillColor: "rgba(76,175,80,0.2)",
	        strokeColor: "rgba(76,175,80,1)",
	        pointColor: "rgba(76,175,80,1)",
	        pointStrokeColor: "#fff",
	        pointHighlightFill: "#fff",
	        pointHighlightStroke: "rgba(76,175,80,1)",
	        data: rawData.data
	    }
	]
	};

    var ctx = document.getElementById("radarDiasSemanaTotal").getContext('2d');
    var myLineChart = new Chart(ctx).Radar(data, option);
}(jQuery));
</script>