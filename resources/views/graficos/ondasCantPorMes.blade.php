<script>
(function ($) { 
    function getRawData(){
        $.ajaxSetup({
          async: false
        });

        var rawData = [];
        $.getJSON("/porMes/"+"{{$id_conv}}", function( data ) {

            _labels = [];
            _data = [];
            data.forEach(function(d) {
                _labels.push(d.mes);
                _data.push(d.cantidad);
            });
        });
        return {labels: _labels, data:_data};
    }

    var option = {
    responsive: true,
    };
   

    rawData = getRawData();
    var data = {
        labels: rawData.labels,
        datasets: [
            /*{
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: rawData.data
            },*/
            {
                label: "My Second dataset",
                fillColor: "rgba(33, 150, 243, 0.2)",
                strokeColor: "rgba(33, 150, 243, 1)",
                pointColor: "rgba(33, 150, 243, 1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(33, 150, 243, 1)",
                data: rawData.data
            }
        ]
    }

    // Get the context of the canvas element we want to select
    var ctx = document.getElementById("ondasCantPorMes").getContext('2d');
    var myLineChart = new Chart(ctx).Line(data, option); //'Line' defines type of the chart.
}(jQuery));
</script>