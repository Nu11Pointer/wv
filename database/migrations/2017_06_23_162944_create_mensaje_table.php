<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensaje', function (Blueprint $table) {
            $table->increments('id_mensaje')->unsigned()->unique();
            $table->integer('n_mensaje')->unsigned();
            $table->date('fecha');
            $table->time('hora');
            $table->string('remitente');
            $table->longText('texto');
            $table->string('id_conv',8);
            $table->foreign('id_conv')->references('id_conv')->on('conversacion')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensaje');
    }
}
