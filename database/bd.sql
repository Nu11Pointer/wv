DROP DATABASE IF EXISTS wv;

CREATE DATABASE wv CHARACTER SET utf8mb4 COLLATE = utf8mb4_unicode_ci;
CREATE DATABASE wv CHARACTER SET utf8mb4 COLLATE = utf8mb4_bin;
CREATE DATABASE wv CHARACTER SET utf8;

#CAMBIAR EN my.ini
[mysqld]
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci # or whatever utf8mb4 collation you choose

CREATE TABLE conversacion(
	id_conv INTEGER unsigned NOT NULL AUTO_INCREMENT,
	fecha_subida DATE NOT NULL,
	owner VARCHAR(16) NOT NULL,
	
	CONSTRAINT pk_conversacion
	PRIMARY KEY (id_conv)
)ENGINE=InnoDB;


CREATE TABLE mensaje(
	id_mensaje INTEGER unsigned NOT NULL AUTO_INCREMENT,
	fecha DATE NOT NULL,
	hora TIME NOT NULL,
	remitente VARCHAR(32) NOT NULL,
	texto TEXT NOT NULL,

	id_conv INTEGER unsigned NOT NULL,

	CONSTRAINT pk_mensaje
	PRIMARY KEY (id_mensaje),

	CONSTRAINT fl_mensaje_conversacion
	FOREIGN KEY (id_conv) REFERENCES conversacion (id_conv)
		ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB;


CREATE TABLE palabra(
	lexema VARCHAR(32) NOT NULL,
	cant INTEGER NOT NULL,

	id_conv INTEGER unsigned NOT NULL,

	CONSTRAINT pk_palabra
	PRIMARY KEY (id_conv, lexema),

	CONSTRAINT fl_palabra_conversacion
	FOREIGN KEY (id_conv) REFERENCES conversacion (id_conv)
		ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB;

#Inserciones
INSERT INTO conversacion(id_conv, fecha_subida, owner) VALUES (NULL, 12/05/2017, "Federico")
INSERT INTO mensaje(id_conv, id_mensaje, fecha, hora, remitente, mensaje) VALUES (1, NULL, STR_TO_DATE('14/3/15','%d/%m/%Y'), STR_TO_DATE('5:29 PM', '%h:%i %p'), "Federico", "Me alegro")
INSERT INTO palabra(lexema, cant, id_conv) VALUES ('hola', 10, 5)

#EXTRA
SHOW VARIABLES WHERE Variable_name LIKE 'character\_set\_%' OR Variable_name LIKE 'collation%';
SHOW VARIABLES LIKE 'lc_time_names';
SET lc_time_names = 'es_AR';



#Cantidad de mensajes entre un rango de hora
select count(*) from mensaje where hora between STR_TO_DATE('4:00 AM', '%h:%i %p') AND STR_TO_DATE('6:00 AM', '%h:%i %p')
#Cantidad de mensajes por remitente
SELECT remitente, COUNT(*) FROM mensaje GROUP BY remitente;
#Cantidad de mensajes por fecha
SELECT fecha, COUNT(*) FROM mensaje GROUP BY fecha;
#Cantidad de mensajes por dia de la semana
SELECT WEEKDAY(fecha), COUNT(*) FROM mensaje GROUP BY WEEKDAY(fecha) ORDER BY WEEKDAY(fecha);
SELECT DAYNAME(fecha) as dia, COUNT(*) as cantidad FROM mensaje GROUP BY dia ORDER BY WEEKDAY(fecha);

#cantidad de mensajes por dia por remitente
SELECT DAYNAME(fecha) as dia, COUNT(*) as cantidad, remitente FROM mensaje GROUP BY dia,remitente ORDER BY WEEKDAY(fecha);

#cantidad de mensajes por mes
SELECT MONTHNAME(fecha) as mes, COUNT(*) as cantidad FROM mensaje GROUP BY mes ORDER BY MONTH(fecha);

#mensajes por hora del dia
SELECT HOUR(hora) as hora, COUNT(*) as cantidad FROM mensaje GROUP BY HOUR(hora);
SELECT HOUR(hora) as hora, remitente, COUNT(remitente) as cantidad FROM mensaje GROUP BY HOUR(hora),remitente ORDER BY HOUR(hora);

#mensajes de una determinada hora
SELECT * FROM mensaje WHERE HOUR(hora) = 6;

#largo del texto
SELECT *,CHAR_LENGTH(texto) FROM mensaje WHERE HOUR(hora) = 6;

#top 10 largo del texto
SELECT *,CHAR_LENGTH(texto) as largo FROM mensaje ORDER BY CHAR_LENGTH(texto) DESC LIMIT 10;

#buscar una palabra
select * from mensaje where texto LIKE '%hola%';

#cantidad de mensajes por numero de dia
SELECT DAYOFYEAR(fecha) as n_dia, COUNT(fecha) as count FROM mensaje WHERE fecha>='2016-01-01' AND fecha<='2016-12-31' GROUP BY fecha;

#ranking palabras
SELECT * FROM palabra ORDER BY cant DESC LIMIT 100;

#risas
select * from palabra where lexema LIKE '%jaj%';
#suma de risas
select sum(cant) as suma from palabra where lexema LIKE '%jaj%';

#si vs no
SELECT * FROM mensaje WHERE texto RLIKE "[[:<:]]si[[:>:]]";

#------------ si vs no ------------
SELECT count(*),

	CASE
        WHEN texto RLIKE "[[:<:]]si[[:>:]]" THEN 'si' WHEN texto RLIKE "[[:<:]]no[[:>:]]" THEN 'no' ELSE NULL
    END as result

FROM mensaje
WHERE 
(texto RLIKE "[[:<:]]si[[:>:]]") OR
(texto RLIKE "[[:<:]]no[[:>:]]")
GROUP BY
    CASE
        WHEN texto RLIKE "[[:<:]]si[[:>:]]" THEN 'si'
        WHEN texto RLIKE "[[:<:]]no[[:>:]]" THEN 'no'
        ELSE NULL
    END;

#resumida
SELECT CASE WHEN texto RLIKE "[[:<:]]si[[:>:]]" THEN 'si' WHEN texto RLIKE "[[:<:]]no[[:>:]]" THEN 'no' ELSE NULL END as palabra, count(*) as cantidad
FROM mensaje
WHERE id_conv=1 AND ((texto RLIKE "[[:<:]]si[[:>:]]") OR (texto RLIKE "[[:<:]]no[[:>:]]"))
GROUP BY 1;
#------------ si vs no ------------

select * from palabra where lexema="si" or lexema="no";

#quien escribe más: cantidad de caracteres (largo del texto)
SELECT remitente,SUM(CHAR_LENGTH(texto)) as cantChar FROM mensaje GROUP BY remitente;

#mensajes por año y mes
select YEAR(fecha), MONTH(fecha), count(*) FROM mensaje GROUP BY YEAR(fecha), month(fecha);
select YEAR(fecha), MONTHNAME(fecha), count(*) FROM mensaje GROUP BY YEAR(fecha), month(fecha);
select remitente, YEAR(fecha), MONTHNAME(fecha), count(*) FROM mensaje GROUP BY YEAR(fecha), month(fecha), remitente;

#remitentes
SELECT DISTINCT remitente FROM mensaje;

#cantidad por dia del año
SELECT DAYOFYEAR(fecha), COUNT(*) as count FROM mensaje GROUP BY fecha;

#cuenta cantidad desde ultimo dia del año
SELECT fecha, COUNT(*) as cant FROM mensaje WHERE fecha >= DATE_SUB(NOW(),INTERVAL 1 YEAR) GROUP BY fecha;

#cantidad y largo
SELECT remitente, SUM(CHAR_LENGTH(texto)) as cantChar, COUNT(*) as count FROM mensaje GROUP BY remitente;

#porcentual
SELECT 
	remitente, 
	COUNT(*)/(SELECT COUNT(*) FROM mensaje WHERE id_conv=135) * 100 AS porc_count, 
	SUM(CHAR_LENGTH(texto))/(SELECT SUM(CHAR_LENGTH(texto)) FROM mensaje WHERE id_conv=135) * 100 AS porc_long 
FROM mensaje
WHERE id_conv=135
GROUP BY remitente;

#remitente y id_conv
SELECT mensaje.id_conv, GROUP_CONCAT(DISTINCT remitente) as remitentes, COUNT(*) as cant, conversacion.nombre_conv FROM mensaje LEFT JOIN conversacion ON (mensaje.id_conv = conversacion.id_conv) GROUP BY mensaje.id_conv ORDER BY mensaje.id_conv DESC;

#conteo de palabras SQL VERVATIM
SELECT SUM(total_count) as total, value
FROM (
SELECT count(*) AS total_count, REPLACE(REPLACE(REPLACE(x.value,'?',''),'.',''),'!','') as value
FROM (
SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(t.texto, ' ', n.n), ' ', -1) value
  FROM mensaje t CROSS JOIN 
(
   SELECT a.N + b.N * 10 + 1 n
     FROM 
    (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
   ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
    ORDER BY n
) n
 WHERE n.n <= 1 + (LENGTH(t.texto) - LENGTH(REPLACE(t.texto, ' ', '')))
 ORDER BY value

) AS x
GROUP BY x.value

) AS y
GROUP BY value
ORDER BY total desc LIMIT 10;


#----------------------------------
SELECT SUM(total_count) as total, LOWER(value) as palabra
FROM (
SELECT count(*) AS total_count, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(x.value,'?',''),'.',''),'!',''),',',''),'(',''),')','') as value
FROM (
SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(t.texto, ' ', n.n), ' ', -1) value
  FROM mensaje t CROSS JOIN 
(
   SELECT a.N + b.N * 10 + 1 n
     FROM 
    (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
   ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
    ORDER BY n
) n
 WHERE n.n <= 1 + (LENGTH(t.texto) - LENGTH(REPLACE(t.texto, ' ', ''))) AND id_conv=1
 ORDER BY value

) AS x
GROUP BY x.value

) AS y
WHERE LOWER(value) NOT IN (SELECT palabra FROM stopword)  
GROUP BY LOWER(value)
ORDER BY total desc LIMIT 100;
#----------------------------------

#cantidad de apariciones de un caracter : emoji!

SELECT remitente, fecha, texto ,ROUND (( LENGTH(texto) - LENGTH( REPLACE (texto, '📍', '') ) ) / LENGTH('📍') ) AS count    
FROM mensaje 
WHERE ROUND (( LENGTH(texto) - LENGTH( REPLACE (texto, '📍', '') ) ) / LENGTH('📍') )>0
LIMIT 10;


SELECT SUM(ROUND (( LENGTH(texto) - LENGTH( REPLACE (texto, '😊', '') ) ) / LENGTH('😊') )) AS count    
FROM mensaje 
WHERE ROUND (( LENGTH(texto) - LENGTH( REPLACE (texto, '😊', '') ) ) / LENGTH('😊') )>0 AND id_conv=2
LIMIT 10;