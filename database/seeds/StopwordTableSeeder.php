<?php

use Illuminate\Database\Seeder;

class StopwordTableSeeder extends Seeder
{
	    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
 	{
        $palabras = file('database/spanish', FILE_IGNORE_NEW_LINES);
 		while(!empty($palabras))
            try{

	        DB::table('stopword')->insert([
	            'palabra' => array_pop($palabras)
	        ]);

            }catch(\Exception $he){
                //Error de insersion, no pasa nada
            }
    }

}