DROP EVENT IF EXISTS eliminarConv1Sem;
DELIMITER !
CREATE EVENT eliminarConv1Sem
  ON SCHEDULE EVERY 1 DAY STARTS '1993-08-17 01:58:00'
  ON COMPLETION PRESERVE
DO BEGIN
   delete from conversacion 
   where datediff(now(),fecha_subida)>7;
END;!
DELIMITER ;