$.ajaxSetup({
  async: false
});

$(function() {
    $("div").lazyload();
    console.log("falta hacer funcionar");
});

function decorarLegend(legend){
/*
<ul class="bar-legend list-group col-md-5">
	<li class="list-group-item" style="background-color:rgba(139, 195, 74, 0.5)">Cantidad de mensajes</li>
	<li class="list-group-item" style="background-color:rgba(76, 175, 80, 0.5)"	>Longitud de mensajes</li>
</ul>
*/

/*
<ul class="bar-legend"><li><span style="background-color:rgba(139, 195, 74, 0.5)">Cantidad de mensajes</span></li><li><span style="background-color:rgba(76, 175, 80, 0.5)">Longitud de mensajes</span></li></ul>

<ul class="bar-legend">
	<li>
		<span style="background-color:rgba(139, 195, 74, 0.5)">Cantidad de mensajes</span>
	</li>
	<li>
		<span style="background-color:rgba(76, 175, 80, 0.5)">Longitud de mensajes</span>
	</li>
</ul>
*/
	l = legend;
	legend = $(legend);
	
	var ul = document.createElement('ul');
	ul.className = "bar-legend list-group text-center";

	legend.children().each(function(){
		
		var li = document.createElement('ul');
		li.className = "list-group-item";

		var color = this.childNodes[0].style.backgroundColor;
		var texto = this.childNodes[0].textContent;

		li.style = "background-color: "+color;
		li.textContent = texto;

		ul.appendChild(li);
	});

	return ul;
}

/* Set the width of the side navigation to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function navOpenClose(){
	var ancho = document.getElementById("mySidenav").style.width;
	if(ancho == "0px")
		openNav();
	else closeNav();
}

function agregarTrasparencia(hex, transparencia){
	return "#"+transparencia + hex.substring(1);
}

function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        //return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',');
    }
    throw new Error('Bad Hex '+hex);
}

$(document).ready(function(){
	$('.count').each(function (){
		
	    $(this).prop('Counter',0).animate({
	        Counter: $(this).text()
	    }, {
	        duration: 4000,
	        easing: 'swing',
	        step: function (now) {
	            $(this).text(Math.ceil(now));
	        }
	    });
	});
});