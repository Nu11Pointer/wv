<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \App\Http\Controllers\ConsultorController;

Route::get('/', function () {
    return view('cargadorArchivo', ['conv' => ConsultorController::convRemitentes()]);
});

Route::get('/visualizacion/{id_conv}', 'ViewController@makeView')->name('visualizacion');

Route::get('/{query}/{id_conv}', 'ConsultorController@ConsultorRouter');
Route::get('/convRemitentes', 'ConsultorController@convRemitentes');

Route::get('/prueba', function(){
	return view('prueba');
});

Route::post('/cargarConversacion','ParserController@cargarConversacion');

Route::get('/mail','MailController@getMails');