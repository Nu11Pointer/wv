<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as R;
use App\Conversacion;
use App\Mensaje;
use File;
use DB;
use Session;
use Carbon;

define("FECHA",0);
define("HORA",1);
define("REMITENTE",2);
define("TEXTO",3);

class ParserController extends Controller
{
    public function cargarConversacion(Request $request){
        DB::disableQueryLog();
        DB::beginTransaction();

    	$file = $request->conversacion;

        $id_conv = ParserController::nuevaConversacion($file->getClientOriginalName(), Session::getId(), R::ip());

        if(!$file)
            dd("Error con el archivo");

        $i = 1;
        // $data = [];
        foreach(file($file->getRealPath(), FILE_IGNORE_NEW_LINES) as $lineaCruda) {
            try{
                $chat = ParserController::splitChat($lineaCruda);
                ParserController::nuevoMensaje($id_conv, $i, $chat);
                // $mensajeArray = ParserController::nuevoMensajeArray($id_conv, $i, $chat);
                // array_push($data,$mensajeArray);
                $i++;
            }catch(\Exception $he){
                //Error de lectura de caracteres
            }
        }
        // Mensaje::insert($data);
        DB::commit();

        return redirect()->route('visualizacion', ['id_conv' => $id_conv]);
    }

    public function cargarPorMail($nombre, $texto, $owner){
        DB::disableQueryLog();
        DB::beginTransaction();

        $id_conv = ParserController::nuevaConversacion($nombre, $owner, 0);

        $i = 1;
        foreach(preg_split("/((\r?\n)|(\r\n?))/", $texto) as $lineaCruda){
            try{
                $chat = ParserController::splitChat($lineaCruda);
                ParserController::nuevoMensaje($id_conv, $i, $chat);
                $i++;
            }catch(\Exception $he){
                //Error de lectura de caracteres
            }
        }
        DB::commit();

        return $id_conv;
    }

    private function nuevaConversacion($nombre, $owner, $ip_owner){
    	$conv = new Conversacion();
    	$conv->id_conv = str_random(8);
    	$conv->fecha_subida = new \DateTime();
    	$conv->owner = $owner;
    	$conv->ip_owner = $ip_owner;
        $conv->nombre_conv = $this->getNombreConv($nombre);
    	$conv->save();

    	return $conv->id_conv;
    }

    public function getNombreConv($nombre_archivo){
        return str_replace(["Chat de WhatsApp con ",".txt"], "", $nombre_archivo);
    }

    private function nuevoMensajeArray($idConv, $nMensaje, $splitChat){
        $toReturn = [];
        $toReturn["n_mensaje"] = $nMensaje;
        $toReturn["fecha"] = $splitChat[FECHA];
        $toReturn["hora"] = $splitChat[HORA];
        $toReturn["remitente"] = $splitChat[REMITENTE];
        $toReturn["texto"] = $splitChat[TEXTO];
        $toReturn["id_conv"] = $idConv;

        return $toReturn;
    }

    private function nuevoMensaje($idConv, $nMensaje, $splitChat){
    	$m = new Mensaje();
    	$m->n_mensaje = $nMensaje;
    	$m->fecha = $splitChat[FECHA];
    	$m->hora = $splitChat[HORA];
    	$m->remitente = $splitChat[REMITENTE];
    	$m->texto = $splitChat[TEXTO];
    	$m->id_conv = $idConv;
       
    	$m->save();
    }

    private function splitChat($chatCrudo){
	// String [] chat = new String[4];
		$chat = [];
	// String[] aux = chatCrudo.split(",",2);
		$aux = explode(",", $chatCrudo, 2);
	// chat[FECHA]=aux[0];
    //Y-m-d
        $auxDate = explode("/",$aux[0]);
		$chat[FECHA] = $auxDate[2]."-".$auxDate[1]."-".$auxDate[0];
	// aux = aux[1].split("-",2);
		$aux = explode("-", $aux[1], 2);
	// chat[HORA]=aux[0].substring(1);
		$chat[HORA] = date("H:i:s",strtotime(substr($aux[0], 1, -1)));
	// aux = aux[1].split(":",2);
		$aux = explode(":", $aux[1], 2);
	// chat[REMITENTE]=aux[0].substring(1);
		$chat[REMITENTE] = substr($aux[0], 1);
	// chat[TEXTO]=aux[1].substring(1).replaceAll("(\"|\\\\)" , ""); //Elimina el primer espacio y si tiene comillas
		$chat[TEXTO] = substr($aux[1], 1);
		return $chat;
    }
}
