<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conversacion;
use App\Mensaje;

class ViewController extends Controller
{
    public function makeView($id_conv){
    	$attr = [
    		'id_conv' => $id_conv,
    		'cant_msg' => Mensaje::where('id_conv',$id_conv)->count()
    	];

    	if ($attr['cant_msg']!=0)
			return view('visualizacion', $attr);
		else return view('conv_no_encontrada');
    }
}
