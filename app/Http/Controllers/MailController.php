<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMail;

class MailController extends Controller{

	public static function getMails(){
		/* connect to gmail */
		$hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
		$username = 'whatsappvisum@gmail.com';
		$password = 'homerojsimpsons';

		/* intento de conexion */
		$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

		/* obteniendo emails */
		$emails = imap_search($inbox,'ALL');

		/* if emails are returned, cycle through each... */
		if($emails) {
			/* put the newest emails on top */
			rsort($emails);
			
			/* for every email... */
			foreach($emails as $email_number) {
				
				/* get information specific to this email */
				// $overview = imap_fetch_overview($inbox,$email_number,0);
				$header = imap_headerinfo($inbox,$email_number);
				// $message = imap_fetchbody($inbox,$email_number,2);
				$structure = imap_fetchstructure($inbox, $email_number);
				
				$owner = $header->from[0]->mailbox."@".$header->from[0]->host;

				if(isset($structure->parts) && count($structure->parts)) {
					for($i = 0; $i < count($structure->parts); $i++) {

					//Si tiene esto tiene adjuntos
					if($structure->parts[$i]->ifdparameters) {
					 	//Para cada 'part'
						foreach($structure->parts[$i]->dparameters as $object) {
					 		if(strtolower($object->attribute) == 'filename')
					    		$nombre = $object->value;
					 		else if (strtolower($object->attribute) == 'name')
					    		$nombre = $object->value;
					    	else continue; //Nunca deberia llegar ya que el adjunto debe tener un nombre

					    	$nombre = imap_utf8($nombre);


					    	if( strcmp( pathinfo($nombre, PATHINFO_EXTENSION), 'txt') != 0)
								break 2;//Encontré el archivo de texto, rompo el if y el foreach
							}

					 		if ($nombre)
					 			$texto = imap_fetchbody($inbox, $email_number, $i+1);
							if ($structure->parts[$i]->encoding == 3) // 3 = BASE64
								$texto = base64_decode($texto);

					 		$parser = new ParserController();
					 		$id_conv = $parser->cargarPorMail($nombre, $texto, $owner);

							MailController::sendMail($owner, $parser->getNombreConv($nombre), $id_conv);
						}
					}
				}

				//Borrar el mensaje analizado
				imap_delete($inbox ,$email_number);
			}
		}

		/* close the connection */
		imap_close($inbox);
	}

	private static function sendMail($remitente, $nombre_conv, $id_conv){
		Mail::send(new sendMail($remitente, $nombre_conv, $id_conv));

		//En vez de text va html para decorar
		/*Mail::send(['html'=>'mail'], ['id_conv' => '1'], function($message){
			$message->to($remitente,'Hecho para ti')->subject('Conversación procesada');
			$message->from('whatsappvisum@gmail.com', 'WhastappVisum');
		});*/
	}
}