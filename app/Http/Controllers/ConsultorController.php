<?php

namespace App\Http\Controllers;

use Request;
use DB;

use App\Mensaje as Mensaje;
use App\Conversacion as Conversacion;

use Session;

class ConsultorController extends Controller
{
    private $id_conv = 0;
    public function consultorRouter($query, $id_conv){
        //Primero verifica si el id conv concuerda con el owner
        // $filaOwner = Conversacion::where(['owner'=> Session::getId(), 'id_conv' => $id_conv])->first();
        // if($filaOwner == null)
        //     return "Acceso denegado";
        
        $this->id_conv = "'".$id_conv."'";
        return call_user_func(__NAMESPACE__ .'\ConsultorController::'.$query, $id_conv);
    }
    public function cantPorRemitente(){
        return DB::select("SELECT remitente, COUNT(*) as cantidad FROM mensaje WHERE id_conv=".$this->id_conv." GROUP BY remitente;");
    }

    public function cantAnioAtras(){
        return DB::select(
            "SELECT fecha, COUNT(*) as count FROM mensaje WHERE id_conv=".$this->id_conv." AND fecha >= DATE_SUB(NOW(),INTERVAL 1 YEAR) GROUP BY fecha;");
    }

    public function largoPorRemitente(){
        return DB::select(
            "SELECT remitente,SUM(CHAR_LENGTH(texto)) as cantChar FROM mensaje WHERE id_conv=".$this->id_conv." GROUP BY remitente;");
    }
    public function porHora(){
        return  DB::select("SELECT HOUR(hora) as hora, COUNT(*) as cantidad FROM mensaje WHERE id_conv=".$this->id_conv." GROUP BY HOUR(hora)");
    }    
    public function porHoraRemitente(){
    	return  DB::select("SELECT HOUR(hora) as hora, remitente, COUNT(remitente) as cantidad FROM mensaje WHERE id_conv=".$this->id_conv." GROUP BY HOUR(hora),remitente ORDER BY HOUR(hora);");
    }
 	public function porFecha(){
		return  DB::select("SELECT fecha, COUNT(*) as cantidad FROM mensaje WHERE id_conv=".$this->id_conv." GROUP BY fecha;");
    }
    public function porMes(){
        return  DB::select("SELECT MONTHNAME(fecha) as mes, COUNT(*) as cantidad FROM mensaje WHERE id_conv=".$this->id_conv." GROUP BY mes ORDER BY MONTH(fecha);");
    }

    public function porDia(){
        return DB::select("SELECT DAYNAME(fecha) as dia, COUNT(*) as cantidad FROM mensaje WHERE id_conv=".$this->id_conv." GROUP BY dia ORDER BY WEEKDAY(fecha);");
    }    

    public function porDiaRemitente(){
    	return DB::select("SELECT DAYNAME(fecha) as dia, COUNT(*) as cantidad, remitente FROM mensaje WHERE id_conv=".$this->id_conv." GROUP BY dia,remitente ORDER BY WEEKDAY(fecha);");
    }

    public function mensajes(){
        return DB::select("SELECT * FROM mensaje WHERE id_conv=".$this->id_conv);
    	return Mensaje::where('id_conv',$this->$id_conv)->take(100)/*->pluck("texto")*/;
    }

    public function remitentes(){
        $query =  DB::select("SELECT DISTINCT remitente FROM mensaje WHERE id_conv=".$this->id_conv);
        $toReturn = [];
        foreach($query as $q)
            array_push($toReturn, $q->remitente);
        return $toReturn;
    }

    public function cantLongPorRemitente(){
        return DB::select(
            "SELECT remitente, SUM(CHAR_LENGTH(texto)) as cantChar, COUNT(*) as count FROM mensaje  WHERE id_conv=".$this->id_conv." GROUP BY remitente;");
    }

    public function porcCantLong(){
        return
        DB::select("SELECT 
            remitente, 
            COUNT(*)/(SELECT COUNT(*) FROM mensaje WHERE id_conv=".$this->id_conv.") * 100 AS porc_count, 
            SUM(CHAR_LENGTH(texto))/(SELECT SUM(CHAR_LENGTH(texto)) FROM mensaje WHERE id_conv=".$this->id_conv.") * 100 AS porc_long 
        FROM mensaje
        WHERE id_conv=".$this->id_conv."
        GROUP BY remitente;");
    }

    //Se utiliza en la vista del cargador
    public static function convRemitentes(){
        //$user_id = Session::getId();

        // $conv =  DB::select("SELECT mensaje.id_conv, GROUP_CONCAT(DISTINCT remitente) as remitentes, COUNT(*) as cant, conversacion.nombre_conv FROM mensaje LEFT JOIN conversacion ON (mensaje.id_conv = conversacion.id_conv) WHERE conversacion.owner='.$ user_id.' GROUP BY mensaje.id_conv ORDER BY mensaje.id_conv DESC;");

        $conv =  DB::select("SELECT mensaje.id_conv, GROUP_CONCAT(DISTINCT remitente) as remitentes, COUNT(*) as cant, conversacion.nombre_conv FROM mensaje LEFT JOIN conversacion ON (mensaje.id_conv = conversacion.id_conv) GROUP BY mensaje.id_conv ORDER BY mensaje.id_conv DESC;");
        
        foreach ($conv as $c)
            $c->remitentes = explode(",", $c->remitentes );
        return $conv;
    }

    //Palabras

    public function vsSiNo(){
        return DB::select("SELECT CASE WHEN texto RLIKE \"[[:<:]]si[[:>:]]\" THEN 'si' WHEN texto RLIKE \"[[:<:]]no[[:>:]]\" THEN 'no' ELSE NULL END as palabra, count(*) as cantidad
            FROM mensaje
            WHERE id_conv=".$this->id_conv." AND ((texto RLIKE \"[[:<:]]si[[:>:]]\") OR (texto RLIKE \"[[:<:]]no[[:>:]]\"))
            GROUP BY 1;");
    }

    public function conteoPalabras(){
        return DB::select("
            SELECT SUM(total_count) as total, LOWER(value) as palabra
            FROM (
            SELECT count(*) AS total_count, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(x.value,'?',''),'.',''),'!',''),',',''),'(',''),')','') as value
            FROM (
            SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(t.texto, ' ', n.n), ' ', -1) value
              FROM mensaje t CROSS JOIN 
            (
               SELECT a.N + b.N * 10 + 1 n
                 FROM 
                (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
               ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
                ORDER BY n
            ) n
             WHERE n.n <= 1 + (LENGTH(t.texto) - LENGTH(REPLACE(t.texto, ' ', ''))) AND id_conv=".$this->id_conv."
             ORDER BY value

            ) AS x
            GROUP BY x.value

            ) AS y
            GROUP BY value
            ORDER BY total desc LIMIT 50;
            ");
    }

    public function conteoSinStopwords(){
        return DB::select("
            SELECT SUM(total_count) as total, LOWER(value) as palabra
            FROM (
            SELECT count(*) AS total_count, REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(x.value,'?',''),'.',''),'!',''),',',''),'(',''),')','') as value
            FROM (
            SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(t.texto, ' ', n.n), ' ', -1) value
              FROM mensaje t CROSS JOIN 
            (
               SELECT a.N + b.N * 10 + 1 n
                 FROM 
                (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
               ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
                ORDER BY n
            ) n
             WHERE n.n <= 1 + (LENGTH(t.texto) - LENGTH(REPLACE(t.texto, ' ', ''))) AND id_conv=".$this->id_conv."
             ORDER BY value

            ) AS x
            GROUP BY x.value

            ) AS y
            WHERE LOWER(value) NOT IN (SELECT palabra FROM stopword)  
            GROUP BY LOWER(value)
            ORDER BY total desc LIMIT 50;
            ");
    }

    public function aparicionesEmoji(){
        return DB::select("
            SELECT remitente, fecha, texto ,ROUND (( LENGTH(texto) - LENGTH( REPLACE (texto, '❤', '') ) ) / LENGTH('❤') ) AS count    
            FROM mensaje 
            WHERE ROUND (( LENGTH(texto) - LENGTH( REPLACE (texto, '❤', '') ) ) / LENGTH('❤') )>0 AND id_conv=".$this->id_conv.";");
    }

    public function sumaEmoji(){
        $emojis = explode(" ", file_get_contents('../database/emoji', FILE_IGNORE_NEW_LINES), 30);
        $toReturn = [];

        while(!empty($emojis)){
            $emoji = array_pop($emojis);
            $query  = DB::select("SELECT SUM(ROUND (( LENGTH(texto) - LENGTH( REPLACE (texto, '".$emoji."', '') ) ) / LENGTH('".$emoji."') )) AS count FROM mensaje WHERE ROUND (( LENGTH(texto) - LENGTH( REPLACE (texto, '".$emoji."', '') ) ) / LENGTH('".$emoji."') )>0 AND id_conv=".$this->id_conv.";");
            
            //Si hay algun emoji de esos
            if($query[0]->count )
                array_push($toReturn, (object) array("emoji"=>$emoji, "total" => $query[0]->count));
        }
        return $toReturn;
    }
}
