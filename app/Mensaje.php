<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    protected $table = 'mensaje';
    public $timestamps = false;
    public $incrementing = true;
    //protected $dateFormat = 'd/m/Y';
}
