<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversacion extends Model
{
    protected $table = 'conversacion';
    public $timestamps = false;
    //const CREATED_AT = 'fecha_subida'; $timestamps = false lo cancela

    public $incrementing = false;
}
