<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    private $remitente;
    private $id_conv;
    private $nombre_conv;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($remitente, $nombre_conv, $id_conv)
    {
        $this->remitente = $remitente;
        $this->id_conv = $id_conv;
        $this->nombre_conv = $nombre_conv;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('mail',['host'=> getHostByName(getHostName()), 'nombre_conv'=>$this->nombre_conv, 'id_conv'=>$this->id_conv])
            // ->from('Whatsapp Visum')
            ->to($this->remitente)
            ->subject('Conversacion procesada');
    }
}
