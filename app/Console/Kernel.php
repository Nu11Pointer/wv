<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\MailCommand::class,
        //
    ];

    /**
     * Define the application's command schedule.
     * Para activar: php artisan schedule:run
     * php C:\Users\Federico\git\wv\artisan schedule:run
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('mail:actualizar')->everyMinute(); 
        //->everyFiveMinutes();->everyTenMinutes();->everyThirtyMinutes();->hourly(); etc
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
