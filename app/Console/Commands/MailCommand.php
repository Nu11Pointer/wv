<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MailController;

class MailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:actualizar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica que existan emails pendientes en la casilla para cargar, y los carga';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        MailController::getMails();
        echo "Casilla de mail verificada";
    }
}
